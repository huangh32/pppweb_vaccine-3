import threading
from PhmWeb.utils.dbconfig import get_PHM_db, get_DW_db
from PhmWeb.biz.reformat_source_data import ReformatSourceData



class DwInsuranceCompany:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._insu_dict = dict()
        self._std_insu_dict = dict()
        self.__init_data()


    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DwInsuranceCompany, "_instance"):
            with DwInsuranceCompany._instance_lock:   #keep thread safe , add lock
                if not hasattr(DwInsuranceCompany, "_instance"):
                    DwInsuranceCompany._instance = DwInsuranceCompany(*args, **kwargs)
        return DwInsuranceCompany._instance

    def __init_data(self):
        dw = get_DW_db()
        all_list = list(dw['Ref_PayerName'].find({}, {'PayerID':1, 'PayerName':1}))
        for item in all_list:
            item['PayerName'] = ReformatSourceData.capitalize_first_letter(item['PayerName'])
            self._insu_dict[item['PayerID']] = item

        zw = get_PHM_db()
        std_list = list(zw['phm_ref_PayerName'].find({}, {'PayerID':1, 'PayerName':1}))
        for item in std_list:
            payer_id = item['PayerID']
            item['PayerName'] = ReformatSourceData.capitalize_first_letter(item['PayerName'])
            self._std_insu_dict[payer_id] = item
            self._insu_dict[payer_id] = item  # update all list


    def get_insurance_company_name(self, payer_id):
        insu_name = payer_id
        if payer_id in self._insu_dict:
            insu_name = self._insu_dict[payer_id]['PayerName']
        # print('get_insurance_company_name payer_id={0}, name={1}'.format(payer_id, insu_name))
        return insu_name

    def get_insurance_company_list_std(self):
        vl_list = list(self._std_insu_dict.values())
        vl_list.sort(key=lambda x: x['PayerName'])
        return vl_list

    def get_insurance_company_list(self):
        vl_list = list(self._insu_dict.values())
        vl_list.sort(key=lambda x: x['PayerName'])
        return vl_list