import threading
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.common import DictUtils

#DbPhmMeasure.instance()


class DbPhmMeasure:
    _instance_lock = threading.Lock()

    def __init__(self):
        print('DbPhmMeasure_init_')
        self.g_measure_list = None
        if self.g_measure_list is None:
            zw = get_PHM_db()
            self.g_measure_list = list(zw['phm_Measure'].find({"ConfigStatus": 1}))

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmMeasure, "_instance"):
            with DbPhmMeasure._instance_lock:   #为了保证线程安全在内部加锁
                if not hasattr(DbPhmMeasure, "_instance"):
                    DbPhmMeasure._instance = DbPhmMeasure(*args, **kwargs)
        return DbPhmMeasure._instance

    #调用方式 DbPhmMeasure.instance().get_instruction('ABA')
    def get_instruction(self, mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                if 'Instruction' in item:
                    return item['Instruction']
                return ''
        return ''

    def get_measure_name(self, mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item['MeasureName']
        return ''

    def get_category(self, mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item['MeasureCategory']
        return ''

    def get_description(self, mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item['Description']
        return ''

    def get_valueset(self,  mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item['Valueset'] if 'Valueset' in item else []
        return []

    #返回True or False
    def is_show_alert(self, mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item['IsShowAlert'] == 1
        return True

    def get_all_info(self,mid):
        for item in self.g_measure_list:
            if item['MeasureID'] == mid:
                return item
        return ''

    def get_list(self, id_list):
        res_list = []
        cat_list = []
        my_list = []
        for item in self.g_measure_list:
            if item['MeasureID'] in id_list:
                my_list.append(item)
                cat_name = item['MeasureCategory']
                if cat_name not in cat_list:
                    cat_list.append(cat_name)
        cat_list.sort()

        for cat in cat_list:
            sub_item = [x for x in my_list if x['MeasureCategory'] == cat]
            sub_item.sort(key=lambda x: x['Ordinal'])
            res_list.extend(sub_item)

        return res_list

    @staticmethod
    def get_total_weight(measure_list):
        weight = 0
        for item in measure_list:
            if item['MeasureID'] == 'AIC':
                continue
            weight += DictUtils.get_int_value(item, 'MeasureWeight')
        return weight

