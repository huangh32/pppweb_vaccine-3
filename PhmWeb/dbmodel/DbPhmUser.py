import threading, datetime
from  bson import ObjectId
from PhmWeb.utils.dbconfig import get_PHM_db
from PhmWeb.biz.redis_cache import RedisCache


class DbPhmUser:
    _instance_lock = threading.Lock()

    def __init__(self):
        pass

    def clear_cache_by_userid(self,user_id):
        RedisCache.instance().clear_phm_user(user_id)

    def get_user(self, user_id):
        cache_user = RedisCache.instance().get_phm_user(user_id)
        if cache_user is None:
            zw = get_PHM_db()
            vo = zw['phm_User'].find_one({'UserId':user_id})
            if vo:
                RedisCache.instance().cache_phm_user(vo)
            return vo
        else:
            return cache_user

    def get_prop_de_identify(self,user_id):
        vo = self.get_user(user_id)
        if vo is not None and 'DeidentifyPatient' in vo:
            return vo['DeidentifyPatient'] == 'On'
        return False

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DbPhmUser, "_instance"):
            with DbPhmUser._instance_lock:  # 为了保证线程安全在内部加锁
                if not hasattr(DbPhmUser, "_instance"):
                    DbPhmUser._instance = DbPhmUser(*args, **kwargs)
        return DbPhmUser._instance

    def save_my_measures(self,user_id, measures):
        zw = get_PHM_db()
        measure_list = measures.split(',')
        zw['phm_User'].update({'UserId': user_id}, {'$set':{'MyMeasureList':measure_list}})
        self.clear_cache_by_userid(user_id)

    def get_my_measures(self, user_id):
        zw = get_PHM_db()
        vo = zw['phm_User'].find_one({'UserId':user_id},{'MyMeasureList':1})
        if vo is not None and 'MyMeasureList' in vo:
            return vo['MyMeasureList']
        return []

    def update_roles(self, user_id, organ_id, my_roles):
        zw = get_PHM_db()
        my_role_list = [ObjectId(x) for x in my_roles]
        role_list = list(zw['phm_syspvg_Role'].find({'OrganId': organ_id}))
        change_flag = False
        for role in role_list:
            rid = role['_id']

            role_users = role.get('Users')
            if role_users is None:
                role_users = list()

            if rid in my_role_list:
                if user_id not in role_users:
                    role_users.append(user_id)
                    zw['phm_syspvg_Role'].update({'_id': rid}, {'$set': {'Users': role_users}})
                    change_flag = True
            else:
                if user_id in role_users:
                    role_users.remove(user_id)
                    zw['phm_syspvg_Role'].update({'_id': rid}, {'$set': {'Users': role_users}})
                    change_flag = True
        return change_flag