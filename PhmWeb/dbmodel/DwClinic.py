import threading, datetime
from PhmWeb.utils.dbconfig import get_DW_db
from PhmWeb.biz.reformat_source_data import ReformatSourceData
from PhmWeb.common import DateUtils


class DwClinic:
    _instance_lock = threading.Lock()

    def __init__(self):
        self._clinic_dict = dict()

    @classmethod
    def instance(cls, *args, **kwargs):
        if not hasattr(DwClinic, "_instance"):
            with DwClinic._instance_lock:   #keep thread safe , add lock
                if not hasattr(DwClinic, "_instance"):
                    DwClinic._instance = DwClinic(*args, **kwargs)
        return DwClinic._instance

    #DwClinic.instance().get_clinic(clinic_id)
    def get_clinic(self, clinic_id):
        if isinstance(clinic_id, str) and len(clinic_id) >0:
            clinic_id = int(clinic_id)

        if clinic_id not in self._clinic_dict:
            dw = get_DW_db()
            clinic = dw['Clinic'].find_one({'ClinicID': clinic_id})
            if clinic:
                ReformatSourceData.format_clinic_name(clinic)
            self._clinic_dict[clinic_id] = clinic
            return clinic
        return self._clinic_dict[clinic_id]

    def get_clinic_name(self, clinic_id):
        cli = self.get_clinic(clinic_id)
        if cli is not None:
            return cli['ClinicName']
        return ''

    def get_office_location(self, clinic_id):
        dw = get_DW_db()
        clinic = dw['Clinic'].find_one({'ClinicID': clinic_id},{'OfficeLocation':1})
        active_location_list = [x for x in clinic['OfficeLocation'] if x['Status'] == 'Active'] if clinic is not None else []
        return active_location_list

    def get_provider_name(self, clinic_id, pro_id):
        dw = get_DW_db()
        pro = dw['Provider'].find_one({'ClinicID':clinic_id, 'EmployeeID': pro_id})
        if pro:
            return '{0}, {1}'.format(pro['FirstName'], pro['LastName'])
        return ''

    def get_provider_name_by_uid(self, uid):
        dw = get_DW_db()
        pro = dw['Provider'].find_one({'EmployeeUID': uid})
        if pro:
            return '{0}, {1}'.format(pro['FirstName'], pro['LastName'])
        return ''

    def get_provider_by_clinic(self, clinic_id):
        dw = get_DW_db()
        if isinstance(clinic_id, str):
            clinic_id = int(clinic_id)

        '''
        pro_list = list(dw['Provider'].find({'ClinicID':clinic_id,
                                             'Disabled':'N','MDLandLock': 'N','ActiveScore':{'$gt':0}
                                             }).sort([('FullName',1)]) )
        '''
        pipe_line = list()
        pipe_line.append({'$match': {'ClinicID':clinic_id,'Disabled':'N','MDLandLock': 'N','NPI':{'$ne':None},'ActiveScore':{'$gt':100}}})
        pipe_line.append({'$match': {'LastLoginDate': {'$gt': DateUtils.get_past_year_date(past_years=1)}} })
        pipe_line.append({'$project':{'ClinicID':1,'ActiveScore':1 ,'NPI':1 ,'Title':1, 'ClinicName':1, 'EmployeeID':1, 'EmployeeUID':1, 'FirstName':1, 'LastName':1, 'FullName':1,
                                      'City':1, 'State':1, 'LastLoginDate':1 , 'NPILength': {'$strLenCP': "$NPI" }} })
        pipe_line.append({'$match':{'NPILength':{'$gt':8} }})
        pipe_line.append({'$sort':{'FullName':1}})
        pro_list = list(dw['Provider'].aggregate(pipe_line))
        return pro_list

    def get_web_id(self, clinic_id):
        cli = self.get_clinic(clinic_id)
        if cli:
            return cli['WebID']
        return -1


if __name__ == '__main__':
    cli_name = DwClinic.instance().get_clinic_name(1642)
    print(cli_name)
