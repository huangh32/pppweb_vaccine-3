import datetime, uuid
from PhmWeb.common import RequestUtils, DateUtils
from PhmWeb.utils.dbconfig import sql_ec_db

class IcCovidVistor:
    def __init__(self):
        self._col_list = list()
        self._val_list = list()

    def register_visitor(self, request, clinic_id):
        self.add_param('ClinicID', str(clinic_id))
        self.add_request_param(request, 'PatientFirstName', 'PatientFirstName')
        self.add_request_param(request, 'PatientLastName', 'PatientLastName')
        self.add_request_param(request, 'PatientDOB', 'PatientDOB', 'Date')
        self.add_request_param(request, 'PatientGender', 'PatientGender')
        self.add_request_param(request, 'PatientAddrStreet', 'PatientAddrStreet')
        self.add_request_param(request, 'PatientAddrApt', 'PatientAddrApt')
        self.add_request_param(request, 'PatientLanguage', 'PatientLanguage')
        self.add_request_param(request, 'PatientAddrCity', 'PatientAddrCity')
        self.add_request_param(request, 'PatientAddrState', 'PatientAddrState')
        self.add_request_param(request, 'PatientAddrZip', 'PatientAddrZip')
        self.add_request_param(request, 'PatientHomePhone', 'PatientHomePhone')
        self.add_request_param(request, 'PatientMobilePhone', 'PatientMobilePhone')
        self.add_request_param(request, 'PatientEmail', 'PatientEmail')
        self.add_request_param(request, 'PCPFlag', 'HasPCP')
        self.add_request_param(request, 'PCPName', 'PCPName')
        self.add_request_param(request, 'PCPNumber', 'PCPPhone')
        self.add_request_param(request, 'InsuredFlag', 'HealthInsurance')
        self.add_request_param(request, 'InsuredID', 'HealthInsuranceID')
        self.add_request_param(request, 'RepInitials', 'RepInitials')
        self.add_param('CreatedDate', DateUtils.datetime_to_long_string(datetime.datetime.now(), same_timezone=True))
        self.add_param('Deleted', '0')
        visitor_id = self.insert_to_db()

        return visitor_id

    def insert_to_db(self):
        cols = ', '.join(self._col_list)
        vals = "','".join(self._val_list)
        vals = "'{0}'".format(vals)
        sql = f"insert into somos_Visitor({cols}) values ({vals});SELECT scope_identity()"
        sql = sql.replace("'NULL'", 'NULL')
        print(sql)
        ec_db = sql_ec_db()
        ret = ec_db.excute(sql)
        ec_db.close_conn()

        vid = 0
        if len(ret) > 0:
            vid = ret[0][0]
        return vid


    def update_patient_id(self, clinic_id, visitor_id, patient_id):
        sql = "update somos_Visitor set PatientID={2} where ClinicID={0} and VisitorID={1}".format(clinic_id, visitor_id, patient_id)
        ec_db = sql_ec_db()
        ec_db.excute_update(sql)
        ec_db.close_conn()


    def add_request_param(self, request, col, key, data_type='String'):
        self._col_list.append(col)
        val = RequestUtils.get_string(request, key)
        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            elif val.find('/') > 0:
                dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                val = DateUtils.datetime_to_short_string(dt)
            elif len(val) == 8:
                dt = DateUtils.string_to_datetime(val)
                val = DateUtils.datetime_to_short_string(dt)
        self._val_list.append(val.replace("'", "''"))


    def add_param(self, col, val):
        self._col_list.append(col)
        self._val_list.append(val)

