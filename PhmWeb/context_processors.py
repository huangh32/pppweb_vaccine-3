from django.conf import settings  # import the settings file

from PhmWeb.common import RequestUtils
from PhmWeb.settings import PHMSettings


def phm_settings(request):
    # return the value you want as a dictionary. you may add multiple values in there.
    return {'PHM_SETTINGS': settings.PHM_SETTINGS,
            "CLINIC_ID": RequestUtils.get_clinic_id_by_host(request)}
