#!/usr/bin/python
#coding:utf-8

from django.shortcuts import render


def index(request):
    #usession = SessionManager.get_session(request)
    #if usession is None or usession.OrganId <= 0:
    #    return render(request, 'root/nopvg.html')
    return render(request, 'docs/index.html',{'OrganID':1})


def docs(request):
    return render(request, 'docs/docs.json')


def module(request, json):
    return render(request, 'docs/%s.json' % json)
