import openpyxl, csv, os
from PhmWeb.common import DateUtils
from PhmWeb.biz.redis_cache import RedisCache

# this will be changed to load to REDIS later
'''
__LOCATIONID_DAY_SLOTS__ = {
'1368_01/15/2021': {'RowNum': 1, 'SiteID': 1368, 'ScheduleDay': '01/15/2021', 'Slots': 175, 'SiteName': 'Brandeis HS (UWS)'},
'1368_01/16/2021': {'RowNum': 2, 'SiteID': 1368, 'ScheduleDay': '01/16/2021', 'Slots': 175, 'SiteName': 'Brandeis HS (UWS)'}

}
'''


def get_location_slot_daily_limit():
    return RedisCache.instance().get_location_slot_daily_limit()


# relative_url = ''
# https://mdland-my.sharepoint.com/:x:/p/steve_peng/ERq3zaCZv25Lkz2J7JK2XoQB2-lgeBDesjJP6ZUKcmVtvg?e=4%3AuhHCxV&at=9

def pass_excel_to_dict():
    fullpath = '../resource/site_vaccine_daily_limit.xlsx'
    wb = openpyxl.load_workbook(fullpath)
    sheet = wb.active
    for excel_sheet_name in wb.sheetnames:
        if excel_sheet_name.lower() == "":
            pass
    rows_header = sheet.iter_rows(min_row=1, max_row=2)  # returns a generator of rows
    column_row = next(rows_header)  # get the first row
    column_list = [c.value for c in column_row]  # extract the values from the cells
    print(f'column_list={column_list}')

    res_dict = dict()
    data_rows = sheet.iter_rows(row_offset=1)
    for idx, row in enumerate(data_rows):
        row = [x.value for x in row]
        site_id = row[1]
        if isinstance(site_id, int) and site_id > 0:
            schedule_day = DateUtils.datetime_to_short_string(row[2])
            site_name = f"{row[5]}" if row[5] else ''
            site_name = site_name.replace("'", '').replace('"', '')
            row_dict = {'RowNum': row[0], 'SiteID': row[1], 'ScheduleDay': schedule_day, 'Slots': row[3], 'SiteName': site_name}
            key = '{0}_{1}'.format(site_id, schedule_day)

            sql_insert = f"""insert into ec67.dbo.somos_VaccineDailyLimit(RowNum,SiteID,ScheduleDay,Slots,SiteName ) values ('{row[0]}', '{row[1]}', '{schedule_day}', '{row[3]}' ,'{site_name}');"""
            print(sql_insert)
            #print(f"'{key}': {row_dict},")
            res_dict[key] = row_dict


if __name__ == '__main__':
    pass_excel_to_dict()


