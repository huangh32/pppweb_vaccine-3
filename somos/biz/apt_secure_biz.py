from PhmWeb.common import RequestUtils, DictUtils
from PhmWeb.utils.dbconfig import sql_ec_db


class AptSecureBiz:
    def __init__(self):
        pass

    '''
    0 - valid
    1 - qid not exists
    2 - qid not match IP
    3 - qid has made appointment
    '''
    def validate_for_select_site(self, qid, request):
        # request from Site Operator
        if RequestUtils.get_string(request, 'command') == 'PhmwebVacccine':
            return 0

        if qid is None or len(qid) != len('f10b35cd-5330-11eb-80cf-005056c00008'):
            return 4

        qs_sql = f" select top 10 BookingRequestIP, QID, AppointmentID from  somos_CovidRegQuestionnaire where QID='{qid}'"
        ec_db = sql_ec_db()
        qs_list = ec_db.fetch_sql_dict_list(qs_sql)
        ec_db.close_conn()
        if qs_list is None or len(qs_list) == 0:
            return 1
        quesn = qs_list[0]
        current_ip = RequestUtils.get_request_remote_addr(request)
        if current_ip != quesn['BookingRequestIP']:
            return 2
        if DictUtils.get_int_value(quesn, 'AppointmentID') > 0:
            return 3
        return 0
