import pymongo, bson
import gridfs
from PhmWeb.utils.dbconfig import get_PHM_db


class GmapMongoCache:
    def __init__(self):
        self._zw = get_PHM_db()
        self._collection = 'gmap_image_cache'

    # save file to mongo
    def save_file_to_mongo(self, file_path, filename, map_hash_key):
        with open(file_path, 'rb') as f:
            data = f.read()
            fs = gridfs.GridFS(self._zw, collection=self._collection)
            return fs.put(data, FileName=filename, MapHashKey=map_hash_key)

    # get bytes from db
    def __get_byte_file_from_mongo(self, id):
        fs = gridfs.GridFS(self._zw, self._collection)
        oid = id if isinstance(id, bson.ObjectId) else bson.ObjectId(str(id))
        gf = fs.get(oid)
        return gf.read()

    # save bytes to file
    def __save_byte_to_disk(self, byte_content, save_to_path):
        with open(save_to_path, 'wb') as f:
            f.write(byte_content)

    def write_to_cache_file_if_exists(self, map_hash_key, cache_to_fullpath):
        file_info = self._zw[self._collection + '.files'].find_one({'MapHashKey': map_hash_key})
        if file_info:
            byte_content = self.__get_byte_file_from_mongo(file_info['_id'])
            self.__save_byte_to_disk(byte_content, cache_to_fullpath)
            return True
        return False


def main():
    biz = GmapMongoCache()
    map_hash_key = 'test_hash_key'
    id = biz.save_file_to_mongo('d:\covid.png', filename='testfilename', map_hash_key=map_hash_key)
    biz.write_to_cache_file_if_exists(map_hash_key=map_hash_key, cache_to_fullpath='d:\covid2.png')
    # write_to_disk(get_file_from_mongo(id))


if __name__ == '__main__':
    main()
