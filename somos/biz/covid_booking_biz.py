import re, datetime
from django.shortcuts import render, redirect
from django.http import JsonResponse
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.ic_covid_visitor import IcCovidVistor
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.dbmodel.ic_patient import IcPatient
from somos.employee_slot import SlotDataService


class CovidBookingBiz:
    def __init__(self):
        self._col_list = list()
        self._val_list = list()

    @staticmethod
    def is_valid_dob(dob):  # mm/dd/yyyy or mmddyyyy
        dt_dob = DateUtils.string_to_datetime(dob)
        print(f'is_valid_dob={dt_dob}')
        if isinstance(dt_dob, datetime.datetime):
            return True
        return False

    def find_existing_visitor(self, clinic_id, request):
        first_name = RequestUtils.get_string(request, 'PatientFirstName')
        last_name = RequestUtils.get_string(request, 'PatientLastName')
        dob = RequestUtils.get_string(request, 'PatientDOB')
        gender = RequestUtils.get_string(request, 'PatientGender')
        # zip = RequestUtils.get_string(request, 'PatientAddrZip')

        dt_dob = DateUtils.string_to_datetime(dob)
        if isinstance(dt_dob, datetime.datetime):
            dob = DateUtils.datetime_to_short_string(dt_dob)

        sql = "select VisitorID from somos_Visitor where ClinicID='{5}' and PatientFirstName='{0}' and PatientLastName='{1}' and PatientDOB='{2}' and PatientGender='{3}'"\
            .format(first_name.replace("'","''"), last_name.replace("'","''"), dob, gender, zip, clinic_id)

        ec_db = sql_ec_db()
        vids = ec_db.excute_select(sql)
        ec_db.close_conn()

        if len(vids) > 0:
            return vids[0][0]
        return 0

    def find_existing_patient(self, clinic_id, request):
        pid = 0
        first_name = RequestUtils.get_safe_sql_string(request, 'PatientFirstName')
        last_name = RequestUtils.get_safe_sql_string(request, 'PatientLastName')
        dob = RequestUtils.get_safe_sql_string(request, 'PatientDOB')
        gender = RequestUtils.get_safe_sql_string(request, 'PatientGender')
        email = RequestUtils.get_safe_sql_string(request, 'PatientEmail')
        # zip = RequestUtils.get_safe_sql_string(request, 'PatientAddrZip')

        # xiao: find patient by email
        # sql = "select PatientID from Patient where ClinicID='{5}' and PatientEmail='{6}' "\
        #    .format(first_name.replace("'","''"), last_name.replace("'","''"), dob, gender, zip, clinic_id, email)
        '''
        Please DO NOT override my logic, same email not means same patient , email is NOT unique 
        Example: you may use the email to register for both yourself and family  member,  
        '''
        if len(dob) == 8:
            dt_dob = DateUtils.string_to_datetime(dob)
            if isinstance(dt_dob, datetime.datetime):
                dob = DateUtils.datetime_to_short_string(dt_dob)

        sql = "select PatientID from Patient where ClinicID='{5}' and PatientFirstName='{0}' and PatientLastName='{1}' and PatientDOB='{2}' and PatientGender='{3}'" \
            .format(first_name.replace("'", "''"), last_name.replace("'", "''"), dob, gender, zip, clinic_id)

        ec_db = sql_ec_db()
        pids = ec_db.excute_select(sql)
        ec_db.close_conn()

        if len(pids) > 0:
            pid = pids[0][0]
        print(f'find_existing_patient={sql}, pid={pid}')
        return pid

    def check_if_email_existed(self, clinic_id, email):

        pid = 0
        sql = f"select PatientID from Patient where ClinicID='{clinic_id}' and PatientEmail='{email}' "
        ec_db = sql_ec_db()
        pids = ec_db.excute_select(sql)
        ec_db.close_conn()

        if len(pids) > 0:
            pid = pids[0][0]
        print(f'find_existing_email={sql}, patientid={pid}')
        return pid

    def find_existing_appointment(self, clinic_id, patient_id):
        sql = "select min(ID) from Appointment where ClinicID='{1}' and PatientID='{0}' and Status in (0,1,7,6) and BeginDateTime between getDate() and DATEADD(DAY, 30, GETDATE()) " \
            .format(patient_id, clinic_id)

        ec_db = sql_ec_db()
        app_ids = ec_db.excute_select(sql)
        print(f'app_ids={app_ids}, sql={sql}')
        ec_db.close_conn()

        if len(app_ids) > 0 and app_ids[0][0] is not None:
            return app_ids[0][0]

        return 0

    def register_patient(self, clinic_id, visitor_id, request):
        ic_pat = IcPatient()
        return ic_pat.register_patient(clinic_id, request, visitor_id)

    def add_appointment(self, clinic_id, patient_id, request, ques_info):
        book_DT = ques_info['BookingPeriod']
        seg_DT = 0
        if isinstance(book_DT, datetime.datetime):
            seg_DT = int((book_DT.hour * 60 + book_DT.minute) / 5)

        if book_DT is None or seg_DT == 0:
            return 0

        self._col_list.clear()
        self._val_list.clear()

        first_name = RequestUtils.get_safe_sql_string(request, 'PatientFirstName')
        last_name = RequestUtils.get_safe_sql_string(request, 'PatientLastName')
        dob = RequestUtils.get_string(request, 'PatientDOB')
        gender = RequestUtils.get_string(request, 'PatientGender')
        patient_name = '{0}, {1} ({2} {3})'.format(last_name.strip(), first_name.strip(), dob, gender)
        visit_type_id = self.get_visit_type(clinic_id)

        self.add_param('ClinicID', str(clinic_id))
        self.add_param('PatientID', str(patient_id))
        self.add_param('PatientName', patient_name)
        self.add_param('BeginDateTime', book_DT.strftime('%Y-%m-%d %H:%M:00.000'))
        self.add_param('EndDateTime', book_DT.strftime('%Y-%m-%d %H:%M:00.000'))
        self.add_param('BeginTime', str(seg_DT))
        self.add_param('EndTime', str(seg_DT))
        self.add_param('BusinessTypeID', str(visit_type_id))
        self.add_param('DoctorID', str(ques_info['BookingDoctorID']))
        self.add_param('LocationID', str(ques_info['BookingLocationID']))
        self.add_param('RequisitionNumber', str(ques_info['QID'])) # 1/14/2021

        # status:6 = Patient Requested
        self.add_param('Status', '6')
        self.add_request_param(request, 'HomePhone', 'PatientHomePhone')
        self.add_request_param(request, 'WorkPhone', 'PatientMobilePhone')


        cols = ', '.join(self._col_list)
        vals = "','".join(self._val_list)
        vals = "'{0}'".format(vals)
        sql = f"insert into Appointment({cols}) values ({vals});SELECT scope_identity()"
        sql = sql.replace("'NULL'", 'NULL')
        print(sql)
        ec_db = sql_ec_db()
        ret = ec_db.excute(sql)
        ec_db.close_conn()

        app_id = 0
        if len(ret) > 0:
            app_id = ret[0][0]
        return app_id

    def get_schedule_interval(self, clinic_id):
        sql = 'SELECT ScheduleInterval FROM Ref_Options WHERE ClinicID={0}'.format(clinic_id)

        ec_db = sql_ec_db()
        ret = ec_db.excute_select(sql)
        ec_db.close_conn()

        interval = 15
        if len(ret) > 0:
            interval = ret[0][0]

        return interval

    def get_visit_type(self, clinic_id):
        sql = 'SELECT BusinessTypeID FROM Business_Type WHERE ClinicID={0} and Status=1 ORDER BY DefaultType desc, BusinessTypeName'.format(clinic_id)

        ec_db = sql_ec_db()
        ret = ec_db.excute_select(sql)
        ec_db.close_conn()

        type_id = 0
        if len(ret) > 0:
            type_id = ret[0][0]

        return type_id

    def add_request_param(self, request, col, key, data_type='String'):
        self._col_list.append(col)
        val = RequestUtils.get_string(request, key)
        if data_type == 'Date':
            if val == '' or val is None:
                val = 'NULL'
            elif val.find('.') > 0:
                dt = datetime.datetime.strptime(val, "%m/%d/%Y")
                val = DateUtils.datetime_to_short_string(dt)
        self._val_list.append(val.replace("'","''"))

    def add_param(self, col, val):
        self._col_list.append(col)
        self._val_list.append(val)
