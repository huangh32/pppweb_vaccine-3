import datetime, json, bson
from collections import OrderedDict
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.templatetags.static import static
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.utils.dbconfig import sql_ec_db
from PhmWeb.dbmodel.DbPhmLogRestAPI import DbPhmLogRestAPI
from PhmWeb.dbmodel.ic_covid_reg_questionnaire import IcCovidRegQuestionnaire
from PhmWeb.settings import PHMSettings
from somos.employee_slot import EmployeeSlot, SlotDataService, SlotUtils
from somos.biz.gmap_utils import GmapUtils
from somos.biz.apt_secure_biz import AptSecureBiz



@csrf_exempt
def index(request):
    action = RequestUtils.get_string(request, 'action')
    if action == 'setSelectedSlot':
        return set_selected_slot(request)
    elif action == 'getSiteWithSlot' or action == 'getSiteWithSlotAPI':
        return get_site_with_slot(request, is_phmweb=(action == 'getSiteWithSlotAPI'))
    else:
        return init_html_page(request)


def init_html_page(request):
    page_dict = dict()
    smg = SessionManager(request)
    page_dict['Lang'] = smg.get_lang_dict()
    page_dict['BookingDay'] = DateUtils.datetime_to_short_string(datetime.datetime.now())
    qid = RequestUtils.get_safe_sql_string(request, 'qid')
    page_dict['qid'] = qid
    page_dict['ValidQidCode'] = AptSecureBiz().validate_for_select_site(qid, request)
    return render(request, 'somos/site_list.html', page_dict)


def set_selected_slot(request):
    res = dict({'ResultCode': 0, 'Errors': ''})
    emp_id = RequestUtils.get_long(request, 'EmpID')
    ol_id = RequestUtils.get_long(request, 'Olid')
    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    book_time = RequestUtils.get_string(request, 'BookTime') # 05/31/2020 10:20:00
    qid = RequestUtils.get_string(request, 'qid')

    channel = RequestUtils.get_string(request, 'Channel') # phmweb_relocate -- not require to validate
    if channel == 'phmweb_relocate':
        biz = IcCovidRegQuestionnaire()
        biz.update_booking_info(qid, emp_id, ol_id, book_time)
        return JsonResponse(res)

    if qid == '':
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-appt-invalid-qid']
        return JsonResponse(res)
    # 05/29202 add validation before locking
    slot_sevice = SlotDataService(clinic_id)
    dt_book_time = datetime.datetime.strptime(book_time, "%m/%d/%Y %H:%M:%S")
    if not slot_sevice.is_available_to_book(location=ol_id, book_time=dt_book_time, my_locker=qid):
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-sites-already-booked']
        return JsonResponse(res)
    # validate questionnair
    valid_qid = AptSecureBiz().validate_for_select_site(qid, request)
    if valid_qid > 0:
        res['ResultCode'] = 1
        res['Errors'] = ['somos-appt-unavailable', 'somos-appt-invalid-qid']
        return JsonResponse(res)

    # double check
    validate_slot_result_code = SlotUtils.is_able_to_commit_appointment(location_id=ol_id, book_time=dt_book_time)
    if validate_slot_result_code > 0:
        res['ResultCode'] = 1
        res['ErrorsCode'] = validate_slot_result_code
        if validate_slot_result_code == 1:
            res['Errors'] = ['somos-appt-unavailable', 'This day is not available any more.']
        elif validate_slot_result_code == 2:
            res['Errors'] = ['somos-appt-unavailable', 'This day has reached its upper limit, please choose another day']
        else:
            res['Errors'] = ['somos-appt-unavailable', 'somos-sites-already-booked']

        # save log to mongdb
        log_dict = dict({'LogCode': 'pppvac_invalid'})
        log_dict.update(res)
        log_dict['Action'] = 'setSelectedSlot'
        log_dict['Qid'] = qid
        log_dict['LocationID'] = ol_id
        log_dict['ClinicID'] = clinic_id
        log_dict['BooKTime'] = book_time
        DbPhmLogRestAPI.instance().add_log(log_dict, request)

        return JsonResponse(res)

    biz = IcCovidRegQuestionnaire()
    biz.update_booking_info(qid, emp_id, ol_id, book_time)
    return JsonResponse(res)


def get_site_with_slot(request, is_phmweb):
    res_dict = dict()
    search_day = RequestUtils.get_string(request, 'day')
    if search_day is None or search_day == '':
        search_day = DateUtils.datetime_to_short_string(datetime.datetime.now())
    dt_search_day = DateUtils.string_to_datetime(search_day)

    step = RequestUtils.get_long(request, 'step')
    if step != 0:
        dt_search_day = dt_search_day + datetime.timedelta(days=step)
        search_day = DateUtils.datetime_to_short_string(dt_search_day)

    res_dict['BookingDay'] = search_day

    # only search recent 50 days
    dt_delta = (dt_search_day - datetime.datetime.now()).days
    print(f'dt_search_day={dt_search_day}, dt_delta={dt_delta}')
    if dt_delta > 35 or dt_delta < -5:
        res_dict['Count'] = 0
        res_dict['Content'] = ''
        return JsonResponse(res_dict)

    address = str(RequestUtils.get_string(request, 'zipcode').strip() or "11106")
    location_id = RequestUtils.get_long(request, 'locationID') # edit appointment can not change site
    location_name = ''

    clinic_id = RequestUtils.get_clinic_id_by_host(request=request)
    slot_data_service = SlotDataService(clinic_id=clinic_id)
    emp_list = slot_data_service.get_em_with_location(location_id=location_id)
    slot_lock_dict = slot_data_service.get_in_lock_appoint(dt_search_day, dt_search_day + datetime.timedelta(days=6))  # all locations' appointment
    emp_vacation_dict = slot_data_service.get_employee_vacation(dt_search_day, dt_search_day + datetime.timedelta(days=6)) # all locations' vaction
    overnight_dict = slot_data_service.get_location_overnight_setting()

    distance_res = slot_data_service.fill_em_with_distance(emp_list, address)
    if distance_res['ResultCode'] > 0:
        return JsonResponse(distance_res)

    emp_gmap_list = distance_res['EmpList']

    json = RequestUtils.get_long(request, 'json')
    if json == 1:
        res_dict['emp_gmap_list'] = emp_gmap_list
        res_dict['slot_lock_dict'] = slot_lock_dict
        res_dict['emp_vacation_dict'] = emp_vacation_dict
        res_dict['LocationName'] = location_name
        res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
        return res_dict

    if is_phmweb:
        mc, num_of_sites, location_name = render_to_phmweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, location_id) # bd.mdland.com , API
    else:
        mc, num_of_sites = render_to_pppweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, overnight_dict) # pp.mdland.com
        if num_of_sites == 0:
            mc = render_to_pppweb_na(emp_gmap_list, search_day)

    res_dict['Content'] = mc
    res_dict['Count'] = num_of_sites
    res_dict['LocationName'] = location_name
    res_dict['GAddr'] = distance_res['Result']["origin_addresses"][0] if 'origin_addresses' in distance_res['Result'] else address
    return JsonResponse(res_dict)


# for https://somosvaccination.mdland.com/  https://rendrcare.mdland.com
def render_to_pppweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, overnight_dict):
    mc = ''
    num_of_sites = 0
    for emp in emp_gmap_list:
        empid = emp['EmployeeID']
        ol = emp['OfficeLocation']
        olid = ol['OLID']

        emp_slot = EmployeeSlot(period_days=5)
        emp_slot.set_week_schedule(emp)  #
        emp_slot.set_inlock_appoint_dict(slot_lock_dict)
        emp_slot.set_employee_vacation(emp_vacation_dict)
        emp_slot.set_overnight_site(overnight_dict)
        slot_interval = SlotUtils.get_schedule_period_by_location(location_id=ol['OLID'])
        day_list = emp_slot.get_slot_by_period(location_id=ol['OLID'], day_begin=search_day, slot_interval=slot_interval)
        if len(day_list) == 0: # all slot of the location are empty
            continue
        num_of_sites += 1
        if PHMSettings.DISABLE_GMAPS_CACHE:
            map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        else:
            map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        mc += '<div class="row" style="margin-left:10px; margin-right: 10px; margin-top:10px; margin-bottom: 50px;" olid={0} siteindex={1}>'.format(ol.get('OLID'), num_of_sites)
        mc += '''<div class="col-lg-3 col-md-3 col-sm-12 g-bg-img-hero g-min-height-50" style="background: url('%s' ) center no-repeat, url('%s') center no-repeat;"></div>''' % (static('images/somos-emblem-marker.png'), map)

        mc += '<div class="col-lg-9 col-md-9 col-sm-12 g-brd-around g-brd-top-around g-brd-gray-light-v3 g-bg-white">'
        mc += '<div class="row" style="height: 100%">'

        senior_only = '<div class="text-danger">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
        # temp_only = '<div class="text-danger">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(olid) in ("1367", "1368", "1369") else ""
        temp_only = ''
        subtitle = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % ol["SiteSubTitle"] if ol.get("SiteSubTitle", None) else ''
        restricted_zip = ""
        restricted_zip_instructions = ""
        olid_zip_enforced = olid in SlotUtils.get_zip_enforced_olids()
        zips_for_olid = SlotUtils.get_olid_zip_dict()[olid]

        if olid_zip_enforced and zips_for_olid:
            restricted_zip = "This location is only for residents in the following zipcodes:"
            restricted_zip += "<br>%s" % ", ".join(zips_for_olid)
            restricted_zip = "<p class='text-muted' style='font-size: smaller;'>%s</p>" % restricted_zip
            restricted_zip_instructions = '<div class="text-danger">Please bring the following as proof of residency: <ul><li>One of the following: State or government-issued ID; Statement from landlord; Current rent receipt or lease; Mortgage records; or</li><li>Two of the following: Statement from another person; Current mail; School records. </li></div>'

        if olid in [1412] and False:
            restricted_zip_instructions += '<label class=" g-color-white g-bg-primary g-font-weight-600 g-px-10 g-py-6">Johnson vaccine</label>'
            restricted_zip_instructions += '&nbsp;<label class="">For adults ages 18 and older</label>'
            restricted_zip_instructions += '''<div style='line-height:1'>Please arrive at the time of your appointment.  If you are more than <b>15 minutes</b> late, your appointment may not be honored,
                                    and you will not receive a vaccine.</div>'''
        elif olid in [1397] and False:
            restricted_zip_instructions += '<label class=" g-color-white g-bg-primary g-font-weight-600 g-px-10 g-py-6">Pfizer vaccine</label>'
            restricted_zip_instructions += '&nbsp;<label class="">For adults ages 16 and older</label>'

        if ol.get('CustomerSiteDescription'):
            restricted_zip_instructions += ol['CustomerSiteDescription']

        mc += f'''
                <div class="col-lg-4 col-md-4 col-sm-12" style="padding: 25px !important">
                <h5>{ol['Name']}</h5>
                {subtitle}
                {restricted_zip}
                <p>{ol['Phone']}</p>
                <p><b>{ol['Address']}, {ol['City']}, {ol['State']} - {ol['Zip']}</b></p>
                <p><b><i class="fa fa-fw fa-map-pin"></i>{emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} <span data-i18n="somos-miles-away"> away</span></b></p>
                 {senior_only}{temp_only}{restricted_zip_instructions}
              </div>
        '''

        mc += '''<div class="col-lg-8 col-md-8 col-sm-12" style="background-color: white; padding-left: 0px; display: flex; justify-content: center">'''
        for day in day_list:
            # print(day)
            mc += '''<div class="col-2" style="background-color: white; margin-left:5px; margin-right: 5px; width: 14%; text-align: center">'''
            mc += f'''<h5 style="margin-top:18px;">{day['Date'].strftime("%A")[:3]}<br><span style="white-space: nowrap;">{day['Date'].strftime("%b")} {day['Date'].day}</span></h5>'''
            mc += '''<div class="row" style="overflow-y: scroll; height: 245px; margin-bottom: 18px;">'''

            if len(day['SlotList']) == 0:
                mc += "<button class='appointment-slot-closed btn disabled btn-sm btn-block u-btn-primary g-color-white g-bg-primary-dark-v1--hover g-font-weight-600 rounded-0' style='margin:5px; cursor: not-allowed;'>NA</button>"
            for slot in day['SlotList']:
                mc += f'''<button class="appointment-slot btn btn-sm btn-block u-btn-primary g-color-white g-bg-primary-dark-v1--hover g-font-weight-600 rounded-0" style="margin:5px; background-color:#65b392;" onclick="selectSlot({empid},{ol['OLID']},'{slot['Value']}')" >{slot['Name']}</button>'''
            mc += '</div></div>'
        mc += '</div>'

        mc += '</div></div>'
        mc += '</div>'

    return mc, num_of_sites


def render_to_pppweb_na(emp_gmap_list, search_day):
    mc = ''
    for ol_index, emp in enumerate(emp_gmap_list):
        empid = emp['EmployeeID']
        ol = emp['OfficeLocation']
        olid = ol['OLID']
        if ol_index > 2: continue  # only show three NA
        if PHMSettings.DISABLE_GMAPS_CACHE:
            map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        else:
            map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        mc += '<div class="row" style="margin-left:10px; margin-right: 10px; margin-top:10px; margin-bottom: 50px;" olid={0} siteindex={1}>'.format(ol.get('OLID'), ol_index)
        mc += '''<div class="col-lg-3 col-md-3 col-sm-12 g-bg-img-hero g-min-height-50" style="background: url('%s' ) center no-repeat, url('%s') center no-repeat;"></div>''' % (static('images/somos-emblem-marker.png'), map)

        mc += '<div class="col-lg-9 col-md-9 col-sm-12 g-brd-around g-brd-top-around g-brd-gray-light-v3 g-bg-white">'
        mc += '<div class="row" style="height: 100%">'
        senior_only = '<div class="text-danger">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
        # temp_only = '<div class="text-danger">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(olid) in ("1367", "1368", "1369") else ""
        temp_only = ''

        mc += f'''
                <div class="col-lg-4 col-md-4 col-sm-12" style="padding: 25px !important">
                <h5>{ol['Name']}</h5>
                <p>{ol['Phone']}</p>
                <p><b>{ol['Address']}, {ol['City']}, {ol['State']} - {ol['Zip']}</b></p>
                <p><b><i class="fa fa-fw fa-map-pin"></i>{emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''} <span data-i18n="somos-miles-away"> away</span></b></p>
                 {senior_only}{temp_only}
              </div>
        '''
        mc += '''<div class="col-lg-8 col-md-8 col-sm-12" style="background-color: white; padding-left: 0px; display: flex; justify-content: center">'''
        dt_begin = DateUtils.string_to_datetime(search_day)
        for idx in range(5):
            dt_day = dt_begin + datetime.timedelta(days=idx)
            day = dict()
            day['Date'] = dt_day
            day['StrDay'] = dt_day.strftime('%m/%d')
            mc += '''<div class="col-2" style="background-color: white; margin-left:5px; margin-right: 5px; width: 14%; text-align: center">'''
            mc += f'''<h5 style="margin-top:18px;">{day['Date'].strftime("%A")[:3]}<br><span style="white-space: nowrap;">{day['Date'].strftime("%b")} {day['Date'].day}</span></h5>'''
            mc += '''<div class="row" style="overflow-y: scroll; height: 245px; margin-bottom: 18px;">'''
            mc += "<button class='appointment-slot-closed btn disabled btn-sm btn-block u-btn-primary g-color-white g-bg-primary-dark-v1--hover g-font-weight-600 rounded-0' style='margin:5px; cursor: not-allowed;'>NA</button>"
            mc += '</div></div>'
        mc += '</div>'

        mc += '</div></div>'
        mc += '</div>'
    return mc


# for https://sv.mdland.com  3553;  https://rendr360.mdland.com 3552, called by API
def render_to_phmweb(emp_gmap_list, slot_lock_dict, search_day, emp_vacation_dict, location_id):
    mc = '<table class="table">'
    num_of_sites = 0
    location_name = ''
    for idx, emp in enumerate(emp_gmap_list):
        empid = emp['EmployeeID']
        ol = emp['OfficeLocation']

        emp_slot = EmployeeSlot(period_days=5)
        emp_slot.set_week_schedule(emp)  #
        emp_slot.set_inlock_appoint_dict(slot_lock_dict)
        emp_slot.set_employee_vacation(emp_vacation_dict)
        slot_interval = SlotUtils.get_schedule_period_by_location(location_id=ol['OLID'])
        day_list = emp_slot.get_slot_by_period(location_id=ol['OLID'], day_begin=search_day, slot_interval=slot_interval)
        if len(day_list) == 0: # all slot of the location are empty
            continue
        num_of_sites += 1
        if PHMSettings.DISABLE_GMAPS_CACHE:
            map = GmapUtils.gmap_img(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        else:
            map = GmapUtils.gmap_img_from_cache(ol['Address'], ol['City'], ol['State'], ol['Zip'], "AIzaSyDq66yIcV7U59_iYKOry5x4qKpHUaoLq8o")
        mc += '<tr olid={0}>'.format(ol.get('OLID'))

        if location_id <= 0:
            mc += f'''<td style="width:400px;height:245px;background: url('https://pp.mdland.com%s' ) center no-repeat, url('https://pp.mdland.com%s') center no-repeat;">''' % (static('images/somos-emblem-marker.png'), map)
            senior_only = '<div class="text-danger">Currently, this site is seeing seniors <b>65+ only</b>.</div>' if ol['SeniorOnly'] else ''
            temp_only = '<div class="text-danger">All vaccine appointments between 9:00am and 12:00pm from 2/12 to 2/21 are ONLY for DOE staff from D75, elementary, and middle schools who are scheduled to work in-person.</div>' if str(ol) in ("1367", "1368", "1369") else ""
            print("temp:%s" % temp_only)
            mc += f'''<div style='background-color:white;height:100px'>
                    <h5>{idx+1}. {ol['Name']}</h5>
                    <div>{ol['Phone']}</div>
                    <div><b>{ol['Address']}, {ol['City']}, {ol['State']} - {ol['Zip']}</b></div>
                    <b>{emp["distance_to_origin"]["distance"]["text"] if 'distance_to_origin' in emp else ''}  away</b>
                     {senior_only}{temp_only}
                    </div>
                  </td>
            '''
        else:
            location_name = ol['Name']

        for day in day_list:
            mc += '''<td style="background-color: white; margin-left:5px; margin-right: 5px; width: 14%; text-align: center">'''
            mc += f'''<h5 style="margin-top:18px; font-weight:bold">{day['Date'].strftime("%A")[:3]}<br><span style="white-space: nowrap;">{day['Date'].strftime("%b")} {day['Date'].day}</span></h5>'''
            mc += '''<div class="row" style="overflow-y: scroll; height: 245px; margin-bottom: 18px;">'''

            if len(day['SlotList']) == 0:
                mc += "<button class='appointment-slot-closed btn disabled btn-sm ' style='margin:5px; cursor: not-allowed;'>NA</button>"
            for slot in day['SlotList']:
                mc += f'''<button class="appointment-slot btn btn-sm btn-primary" style="margin:5px; background-color:#65b392;" onclick="selectSlot({empid},{ol['OLID']},'{slot['Value']}')" >{slot['Name']}</button>'''
            mc += '</div></td>'
        mc += '</tr>'
    mc += '</table>'
    return mc, num_of_sites, location_name


