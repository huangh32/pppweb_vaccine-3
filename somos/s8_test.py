from datetime import datetime, timedelta
from urllib import parse
from django.shortcuts import render,  redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from PhmWeb.biz.sessionmanager import SessionManager
from PhmWeb.common import DateUtils, RequestUtils
from PhmWeb.utils.dbconfig import sql_ec_db
from somos.biz.gmap_utils import GmapUtils
from PhmWeb.biz.environment_variables import EnvironmentVariables

@csrf_exempt
def index(request):
    page_dict = dict({'ResultCode': 0})
    env = dict()
    env['TestEnv'] = EnvironmentVariables.ENVIRONMENT_TEST
    env['ECLINIC_DB67_IP'] = EnvironmentVariables.ECLINIC_DB67_IP
    env['ECLINIC_DB67_IP'] = EnvironmentVariables.ECLINIC_DB67_IP

    mc = ''
    for key in env:
        mc += f'{key}={env[key]} </br>'
    page_dict['MC'] = mc
    return render(request, 'somos/s8_test.html', page_dict)


